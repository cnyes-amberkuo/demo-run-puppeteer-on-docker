const convict = require('convict');

const conf = convict({
  s3Bucket: {
    doc: 'The name of s3 bucket',
    format: String,
    default: null,
    env: 'S3_BUCKET',
  },
});

conf.loadFile('./local.json');
conf.validate({ allowed: 'strict' });

module.exports = conf;
