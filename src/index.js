const puppeteer = require('puppeteer');
const S3 = require('aws-sdk/clients/s3');
const config = require('./config');

class Storage {
  constructor({ bucket }) {
    const params = {
      apiVersion: '2006-03-01',
      params: {
        Bucket: bucket,
      },
    };

    this.s3 = new S3(params);
  }

  upload = ({ name, file, contentType }) =>
    new Promise((resolve, reject) => {
      const key = encodeURIComponent(name);
      const params = {
        Key: key,
        Body: file,
        ContentType: contentType,
        ACL: 'public-read',
      };

      this.s3.putObject(params, (error, data) => {
        if (error) {
          reject(error);
        } else {
          resolve(data);
        }
      });
    });

  delete = ({ name }) =>
    new Promise((resolve, reject) => {
      const key = encodeURIComponent(name);
      const params = {
        Key: key,
      };

      this.s3.deleteObject(params, (error, data) => {
        if (error) {
          reject(error);
        } else {
          resolve(data);
        }
      });
    });
}

class Photographer {
  _isInit = false;
  _browser;
  _page;

  init = async () => {
    this._browser = await puppeteer.launch({
      headless: true,
      args: [
        '--no-sandbox',
      ],
    });
    this._page = await this._browser.newPage();
    this._isInit = true;
  };

  destroy = async () => {
    this._browser = await this._browser.close();
    this._isInit = false;
  };

  isInit = () => this._isInit;

  setPass = (name, value) => this._page.setExtraHTTPHeaders({ [name]: value });

  screenshot = async ({ url, size, type, quality }) => {
    const page = this._page;

    await page.setViewport(size);
    await page.goto(url);

    const image = await page.screenshot({
      type,
      quality,
    });

    return image;
  };
}

(async () => {
  const photographer = new Photographer();
  const storage = new Storage({ bucket: config.get('s3Bucket' )});

  try {
    await photographer.init();

    const image = await photographer.screenshot({
      url: 'https://invest.anue.com',
      type: 'jpeg',
      size: { width: 800, height: 600 },
    });

    await storage.upload({ name: 'test.jpg', file: image }).catch(console.log);
    await photographer.destroy();
  } catch(error) {
    console.error(error);
  }
})();
